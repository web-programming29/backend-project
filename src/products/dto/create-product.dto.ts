import { Length, IsNotEmpty, IsPositive } from 'class-validator';

export class CreateProductDto {
  @Length(4, 50)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  price: number;
}
